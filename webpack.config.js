//var path = require("path");
var webpack = require('webpack');

module.exports = {
    entry: './resources/assets/js/app.js',
    output: {
        path: './public/js',
        filename: 'bundle.js',
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'ng-annotate-loader!babel-loader',
            },
            {
                test: /\.scss/,
                loader: 'style-loader!css-loader!postcss-loader!sass-loader',
            },
            {
                test: /\.css/,
                loader: 'style-loader!css-loader!postcss-loader',
            },
            {test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)$/, loader: 'file-loader'},
            {test: /\.html$/, loader: 'raw-loader'},
            {test: require.resolve('jquery'), loader: 'expose-loader?$!expose-loader?jQuery'},
        ]
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            options: {
                postcss: {
                    plugins: [
                        require('autoprefixer')
                    ]
                }
            }
        })
        ],
    target: 'web'
};

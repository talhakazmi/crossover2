<!DOCTYPE html>
<html ng-app="crossover">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Laravel</title>

</head>
<body>

<ui-view></ui-view>

<div class="footer">
    <!--Created for Crossover assessment-->
    Event management
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZMtrLIJH2iA0k2rGaST9X99CUEAK_8_8"></script>
<script type="text/javascript" src="{{URL::to('js/bundle.js')}}"></script>
</body>
</html>

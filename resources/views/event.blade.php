<p>Hello {{ $company->name }},</p>
<p>Your event is ended today, following are the visitors attended your stand.</p>

@foreach ($visitors as $visitor)
    <p><strong>{{ $visitor->name }}</strong>:{{ $visitor->occupation }}.</p>
@endforeach

<p>Have a good day!</p>
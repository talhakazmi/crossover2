//Import dependencies
//babel import used to get libraries
//JQuery library
import 'jquery';
//Bootstrap library for designing
import 'bootstrap/dist/js/bootstrap.min.js'
//Angular js library for angularJS functionality
import "angular";
//Angular router to manage routing with in angularJS
import 'angular-ui-router';
//Angular local storgae for storing login user.
import 'angular-local-storage'
//Application css
import '../sass/app.scss';
//Jquery date picker library for selecting date in event creation
import '@fengyuanchen/datepicker';
//Jquery datepicker css
import '@fengyuanchen/datepicker/dist/datepicker.css';
//File upload library for uploading company logo and document.
import 'ng-file-upload';
//Import controllers class
//Home controller for managing home screen signup and signin buttons
import HomeCtrl from './app/modules/home/home.ctrl';
//Sign in controller for sign in screen with form and error messages
import SigninCtrl from './app/modules/company/signin/signin.ctrl';
//Sign up controller for sign up screen with form and error messages
import SignupCtrl from './app/modules/company/signup/signup.ctrl';
//Sign out controller for sign out functionality and redirection
import SignoutCtrl from './app/modules/home/signout.ctrl';
//Client map controller for map visiblity with events to client
import MapCtrl from './app/modules/company/map/map.ctrl';
//Hall controller for events hall with available stands
import HallCtrl from './app/modules/company/hall/hall.ctrl';
//Stand controller for Book stand screen with form and error messages
import StandCtrl from './app/modules/company/stand/stand.ctrl';
//Manage stand controller for Manage stand screen with booked stands
import ManageCtrl from './app/modules/company/manage/manage.ctrl';
//Visitors controller for adding ivistors to the stand via form
import VisitorCtrl from './app/modules/company/manage/visitor.ctrl';
//Admin map controller for map visiblity with feature of adding new events to the map.
import AdminMapCtrl from './app/modules/admin/map/map.ctrl';
//Admin Hall controller for events hall with functionality to add available stands
import AdminHallCtrl from './app/modules/admin/hall/hall.ctrl';
//Admin Sign up controller for sign up screen with form and error messages
import AdminSignupCtrl from './app/modules/admin/signup/signup.ctrl';
//Import Service
//Auth controller to maintain logged in user data across application
import AuthCtrl from './app/service/auth.ctrl';
//Import config class
//Application configuration
import config from './app.config';
//application created with a name crossover with initially loaded modules defined below:
//ui.router for routing on angularJS single page application.
//LocalStorageModule for local storeage to be available with in the app.
//ngFileUpload for handling file upload to the application via ajax.
angular
    .module('crossover', [
        'ui.router',
        'LocalStorageModule',
        'ngFileUpload'
    ])
    //loading config from other file
    .config(config)
    //loading/registering controller classes
    .controller('HomeCtrl', HomeCtrl)
    .controller('SignupCtrl',SignupCtrl)
    .controller('SigninCtrl',SigninCtrl)
    .controller('SignoutCtrl',SignoutCtrl)
    .controller('MapCtrl',MapCtrl)
    .controller('HallCtrl',HallCtrl)
    .controller('StandCtrl',StandCtrl)
    .controller('ManageCtrl',ManageCtrl)
    .controller('VisitorCtrl',VisitorCtrl)
    .controller('AdminMapCtrl',AdminMapCtrl)
    .controller('AdminHallCtrl',AdminHallCtrl)
    .controller('AdminSignupCtrl',AdminSignupCtrl)
    //creating component
    .component('header', {
        //html used for templating gmap component
        template: require('./app/component/header/header.html'),
        //default controller for gmap component
        controller: require('./app/component/header/header.ctrl').default,
        //bind custom methods and properties with gmap component
        bindings: {
            //One way binding to check whether active user had any booked stand or not.
            activeUser: '<',
            //Two way binding to to update user when ever its is_admin flag changed.
            isAdmin: '='
        }
    })
    .component('gmap', {
        //html used for templating gmap component
        template: require('./app/component/gmap/gmap.html'),
        //default controller for gmap component
        controller: require('./app/component/gmap/gmap.ctrl').default,
        //bind custom methods and properties with gmap component
        bindings: {
            //Callback on every click on map to display form to add new event in case of admin.
            onClick: '&',
            //Callback on every click on map marker to display its event information.
            onMarkerClick: '&',
        }
    })
    .component('hallGrid', {
        //html used for templating hallGrid component
        template: require('./app/component/hall/hall.html'),
        //default controller for hallGrid component
        controller: require('./app/component/hall/hall.ctrl').default,
        //bind custom methods and properties with hallGrid component
        bindings: {
            //Callback on every click on cell/stand.
            onCellClick: '&',
            //One Way bind for stand to display form to book stand
            showForm: '<',
            //Rows is an input with value doesn't change
            rows: '<',
            //Cols is an input with value doesn't change
            cols: '<',
            //Two way binding for every stand to be available or not.
            activeCell: '=',
        }
    })
    .service('$auth', AuthCtrl);

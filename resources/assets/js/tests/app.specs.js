describe('crossover',function(){

    //Test angular object to be found
    it('contains angular', ()=>{
        expect(typeof angular).toBe("object");
    });
    //Test module function to be found
    it('has a module function', ()=>{
        expect(typeof module).toBe("function");
    });
    //Test inject function to be found
    it('has an inject function', ()=>{
        expect(typeof inject).toBe("function");
    });

});

/*
Sign in and Sign up test cases described
 */
describe('sign in  and sign up controllers',()=>{

    var signInController;
    var signUpController;
    var state;
    var _$httpBackend;
    var _$scope;

    //Load module to get registered controllers for testing
    beforeEach(module('crossover'));

    //Before Each it()/test inject must be called
    beforeEach(inject(($controller,$http,$rootScope, $httpBackend,$state,localStorageService)=>{

        state = $state;
        _$scope = $rootScope.$new();
        _$httpBackend = $httpBackend;
        //Load sign in controller to signInController to perform tests with given injection
        signInController  =  $controller('SigninCtrl',{$http:$http, $scope: _$scope, $state:$state, localStorageService:localStorageService});

    }));

    //Sign in success test
    it('should have logged in and redirected', ()=>{
        //put a check on redirection after successfully login
        spyOn(state, 'go');
        //Create sign in request with expected response on success
        _$httpBackend.expectPOST('api/signin')
            .respond({status: 'success', data: [{}]});

        //Call submit method to run the script
        signInController.submit();

        //To flush pending request
        _$httpBackend.flush();
        //Redirection have been called
        expect(state.go).toHaveBeenCalled();

    });

    //Sign in failure test
    it('should have failed to login', ()=>{
        //put a check to verify no redirection after login request failure
        spyOn(state, 'go');

        //Create sign in request with expected response on failure
        _$httpBackend.expectPOST('api/signin')
            .respond({status: 'failed', error: [{email: 'Invalid Credentials'}]});

        //Call submit method to run the script
        signInController.submit();

        //To flush pending request
        _$httpBackend.flush();
        //Redirection have not been called
        expect(state.go).not.toHaveBeenCalled();
        //Error message returns in case of invalid credentials
        expect(signInController.error[0].email).toBe('Invalid Credentials');
    });

    //Before Each it()/test inject must be called
    beforeEach(inject(($controller,$http,$rootScope, $httpBackend,$state,localStorageService)=>{

        state = $state;
        _$scope = $rootScope.$new();
        _$httpBackend = $httpBackend;
        //Load sign up controller to signUpController to perform tests with given injection
        signUpController  =  $controller('SignupCtrl',{$http:$http, $scope: _$scope, $state:$state, localStorageService:localStorageService});

    }));

    //Sign up success test
    it('should have register successfully and redirect', ()=>{
        //put a check on redirection after successful Sign up
        spyOn(state, 'go');
        //Create sign up request with expected response on success
        _$httpBackend.expectPOST('api/signup')
            .respond({status: 'success', data: [{}]});
        //Call submit method to run the script
        signUpController.submit();
        //To flush pending request
        _$httpBackend.flush();
        //Redirection have been called
        expect(state.go).toHaveBeenCalled();

    });

    //Sign up failure test
    it('should not have register successfully and respond status failed', ()=>{
        spyOn(state, 'go');
        //Create sign up request with expected response on failure
        _$httpBackend.expectPOST('api/signup')
            .respond({status: 'failed', data: [{}]});
        //Call submit method to run the script
        signUpController.submit();
        //To flush pending request
        _$httpBackend.flush();
        //Redirection have not been called
        expect(state.go).not.toHaveBeenCalled();

    });

});
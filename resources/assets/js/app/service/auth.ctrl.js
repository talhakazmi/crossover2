//Class AuthCtrl for storing logged in user's data to use across application.
//location: resources/assets/js/app/service
class AuthCtrl {
    //Constructor with localstorage to be used
    constructor(localStorageService){
        //Assinging local storage to this.localStorageService to access with in the class
        this.localStorageService = localStorageService;
    }

    //method to be used in signin or signup methods.
    setLocalStorage(data){
        //console.log(data);
        //Set data to local storage by key value.
        this.localStorageService.set('id',data.data[0].id);
        this.localStorageService.set('name',data.data[0].name);
        this.localStorageService.set('email',data.data[0].email);
        this.localStorageService.set('token',data.token);
        this.localStorageService.set('admin',data.data[0].is_admin);
        //if admin logged in than no there will be no stands to manage
        if(data.data[0].is_admin) {
            this.localStorageService.set('stands', 0);
        }else {
            //Total number of stands previously booked by the company.
            this.localStorageService.set('stands', data.data[0].stands_count);
        }
    }
}
//export AuthCtrl to angularJS app to register a service.
export default AuthCtrl;
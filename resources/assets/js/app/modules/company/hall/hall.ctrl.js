//Class HallCtrl for events hall presentation screen with stand blocks.
//location: resources/assets/js/app/modules/company/hall
class HallCtrl {
    //Load constructor with
    /*
     *   $http: for ajax call to api
     *   $state: to redirect to any required route
     *   $stateParams: Capture $state query string
     *   localStorageService: passing data to local storage
     */
    constructor($http, $stateParams, $state, localStorageService) {
        //Decorator that wraps a class in a DI injector for the given dependencies.
        'ngInject';
        //Capture event id from query string
        this.eventId = $stateParams.eventId;
        this.$http = $http;
        this.$state = $state;
        this.localStorageService = localStorageService;
        //bind addStand bindings with this.addStand
        this.addStand = this.addStand.bind(this);
        //check if user is logged in or not
        if(!this.localStorageService.get('token'))
        {
            this.$state.go('home');
        }
    }

    //Method to run on initialization
    $onInit() {
        //Ajax call to API with event id
        this.$http({
            method: 'GET',
            url: 'api/stands/get/' + this.eventId,
            headers: {
                'Authorization': 'bearer '+this.localStorageService.get('token')
            }
        }).then(response => {
                //if success event's stands detail fetched
                if(response.data.status == 'success') {
                    this.data = response.data.data;
                }else
                {
                    //else redirect back to map screen
                    this.$state.go('eventsMap');
                }
            })
            .catch(error => {
                //Session expiration error
                if(error.data.error == 'token_expired')
                {
                    this.$state.go('signout');
                }
            })
    }

    //addStand method to be called on book your stand
    addStand(data) {
        if (data.id && !data.company) {
            //IF stand is not already booked than go to add stand information screen with event and stand ids
            this.$state.go('eventsStand', {id: data.id, event: this.eventId});
        }
    }
}

//export class to ui.router state and register a controller to angularjs app
export default HallCtrl;
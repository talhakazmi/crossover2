//Class StandCtrl for events add stand screen with form fields.
//location: resources/assets/js/app/modules/company/stand
class StandCtrl {
    //Load constructor with
    /*
     *   $http: for ajax call to api
     *   $state: to redirect to any required route
     *   $stateParams: Capture $state query string
     *   localStorageService: passing data to local storage
     *   Upload: ngFileUpload to upload file via ajax
     */
    constructor($http, $stateParams, $state, localStorageService, Upload){
        //Decorator that wraps a class in a DI injector for the given dependencies.
        'ngInject';
        //Capture event id from query string
        this.eventId = $stateParams.event;
        //Capture stand id from query string
        this.standId = $stateParams.id;
        this.$http = $http;
        this.$state = $state;
        this.localStorageService = localStorageService;
        this.Upload = Upload;
        //bind submit bindings with this.submit
        this.submit = this.submit.bind(this);
        //data object with empty details to fill after ajax call
        this.data = {
            company: '',
            document,
            logo: '',
            email: ''
        };
        //error object with empty details to fill after ajax call
        this.error = {
            company: '',
            document: '',
            logo: '',
            email: ''
        };
    }

    //Submit method to be called on form submit
    //Two files to be uploaded with submitted form documentFile and logoFile
    submit(documentFile,logoFile){
        //Ajax call to add company to stand
        documentFile.upload = this.Upload.upload({
                url: 'api/stands/update/' + this.standId,
                data: {
                    user_id: this.localStorageService.get('id'),
                    company: this.data.company,
                    email: this.data.email,
                    document: documentFile,
                    logo: logoFile
                },
                headers: {
                    'Authorization': 'bearer '+this.localStorageService.get('token')
                },
            });
        //ajax response
        documentFile.upload.then((response)=>{
            //If successredirect back to event's hall with event id
            if(response.data.status == 'success')
                {
                    this.$state.go('eventsHall', { eventId:  this.eventId});
                    //Add stand count in  local storage
                    this.localStorageService.set('stands',1);
                    console.log(this.localStorageService.get('stands'));
                }else
                {
                    //error returned from service
                    this.error = response.data.error;
                }
            }, (error)=>{
                //Session expiration error
                if(error.data.error == 'token_expired')
                {
                    this.$state.go('signout');
                }
            }, (evt)=>{
                //to Display uploading progress
                //file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });

    }
}

//export class to ui.router state and register a controller to angularjs app
export default StandCtrl;
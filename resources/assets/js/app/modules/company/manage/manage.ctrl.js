class ManageCtrl {
    constructor($http, $stateParams, $state, localStorageService) {
        'ngInject';
        this.$http = $http;
        this.$state = $state;
        this.localStorageService = localStorageService;
        this.addInfo = this.addInfo.bind(this);
        //console.log(this.localStorageService.get('token'));
        //console.log(this.localStorageService.get('id'));
        //console.log(this.localStorageService.get('stands'));
        if(!this.localStorageService.get('token'))
        {
            this.$state.go('home');
        }
    }

    $onInit() {
        this.$http({
            methd: 'GET',
            url: 'api/stands/' + this.localStorageService.get('id'),
            headers: {
                'Authorization': 'bearer '+this.localStorageService.get('token')
            }
        }).then(response => {
            if(response.data.status == 'success') {
                this.data = response.data.data;
            }else
            {
                this.$state.go('eventsMap');
            }
        })
        .catch(error => {
            if(error.data.error == 'token_expired')
            {
                this.$state.go('signout');
            }
        })
    }

    addInfo(data) {
        if (data.id && data.company) {
            this.$state.go('visitor', {id: data.id});
        }
    }
}

export default ManageCtrl;
class VisitorCtrl {
    constructor($http, $stateParams, $state, localStorageService) {
        'ngInject';
        this.standId = $stateParams.id;
        this.$http = $http;
        this.$state = $state;
        this.localStorageService = localStorageService;
        this.data = {
            name: '',
            email: '',
            occupation: ''
        };
        this.error = {
            name: '',
            email: '',
            occupation: ''
        };
        this.message = '';
        this.submit = this.submit.bind(this);
        //console.log(this.localStorageService.get('token'));
        if(!this.localStorageService.get('token'))
        {
            this.$state.go('home');
        }
    }

    submit() {
        this.$http({
            method: 'POST',
            url: 'api/stands/visitor/' + this.standId,
            headers: {
                'Authorization': 'bearer '+this.localStorageService.get('token')
            },
            data: this.data
        }).then(response => {
                if(response.data.status == 'success')
                {
                    this.message = 'Visitor added';
                    this.data = {
                        name: '',
                        email: '',
                        occupation: ''
                    };
                }else{
                    this.error = response.data.error;
                }
            })
            .catch(error => {
                if(error.data.error == 'token_expired')
                {
                    this.$state.go('signout');
                }
            })
    }
}

export default VisitorCtrl;
//Class SignupCtrl for signup screen with form fields.
//location: resources/assets/js/app/modules/company/signup
class SignupCtrl {
    //Load constructor with
    /*
     *   $http: for ajax call to api
     *   $state: to redirect to any required route
     *   $auth: centralize service to pass data to local storage
     */
    constructor($http, $state, $auth){
        //Decorator that wraps a class in a DI injector for the given dependencies.
        'ngInject';
        this.$state = $state;
        this.$http = $http;
        this.$auth = $auth;
        //data object with empty name, email and password to fill after ajax call
        this.data = {
            name: '',
            email: '',
            password: ''
        };
        //error object with empty name, email and password to fill after ajax call
        this.error = {
            name: '',
            email: '',
            password: '',
        };
    }

    //Submit method to be called on form submit
    submit(){
        //Ajax call to API with user provided name, email and password
        this.$http.post('api/signup',this.data)
            .then(response => {

                if(response.data.status == 'success')
                {
                    //on success set localstorage using auth service
                    this.$auth.setLocalStorage(response.data);
                    //normal user sign up and redirects to clients map
                    this.$state.go('eventsMap');
                }
                else
                {
                    //in cas eof error no redirection just display errors
                    this.error = response.data.error;
                }
            })
    }
}
//export class to ui.router state and register a controller to angularjs app
export default SignupCtrl;
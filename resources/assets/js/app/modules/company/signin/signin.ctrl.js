//Class SigninCtrl for signin screen with form fields.
//location: resources/assets/js/app/modules/company/signin
class SigninCtrl {
    //Load constructor with
    /*
     *   $http: for ajax call to api
     *   $state: to redirect to any required route
     *   $auth: centralize service to pass data to local storage
     */
    constructor($http, $state, $auth){
        //Decorator that wraps a class in a DI injector for the given dependencies.
        'ngInject';
        this.$state = $state;
        this.$http = $http;
        this.$auth = $auth;
        //data object with empty email and password to fill after ajax call
        this.data = {
            email: '',
            password: ''
        };
        //error object with empty email and password to fill after ajax call
        this.error = {
            email: '',
            password: '',
        };
    }

    //Submit method to be called on form submit
    submit(){
        //Ajax call to API with user provided email and password
        this.$http.post('api/signin',this.data)
            .then(response => {
                if(response.data.status == 'success')
                {
                    //on success set localstorage using auth service
                    this.$auth.setLocalStorage(response.data);
                    console.log(response.data.data[0].is_admin);
                    if(response.data.data[0].is_admin) {
                        //if logged in user is admin than redirects to admin map
                        this.$state.go('adminMap');
                    }else{
                        //if normal user sign in than redirects to clients map
                        this.$state.go('eventsMap');
                    }
                }
                else
                {
                    //in cas eof error no redirection just display errors
                    this.error = response.data.error;
                }
            })
    }
}
//export class to ui.router state and register a controller to angularjs app
export default SigninCtrl;
//Class MapCtrl for map screen with map and markers if event exists.
//location: resources/assets/js/app/modules/company/map
class MapCtrl {
    //Load constructor with
    /*
     *   $state: to redirect to any required route
     *   localStorageService: passing data to local storage
     */
    constructor($state, localStorageService) {
        //Decorator that wraps a class in a DI injector for the given dependencies.
        'ngInject';
        this.$state = $state;
        this.localStorageService = localStorageService;
        //bind onMarkerClick bindings with this.onMarkerClick
        this.onMarkerClick = this.onMarkerClick.bind(this);
        //get users already booked stands count
        this.stands = localStorageService.get('stands');
        //if localstorage token is not set
        if(!this.localStorageService.get('token'))
        {
            //redirect user to home screen to signin or signup again
            this.$state.go('home');
        }
    }

    //onMarkerClick method to be called when click on marker
    onMarkerClick(data) {
        //Pass event data to event
        this.event = data;
    }
    //bookPlace method to be called when bookPlace button activated and clicked
    bookPlace() {
        //Redirect user to event's hall with event id.
        this.$state.go('eventsHall', {eventId:this.event.id});
    }
}

//export class to ui.router state and register a controller to angularjs app
export default MapCtrl;
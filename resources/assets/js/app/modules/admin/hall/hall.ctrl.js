//Class HallCtrl for events hall presentation screen with stand blocks.
//location: resources/assets/js/app/modules/admin/hall
class HallCtrl {
    //Load constructor with
    /*
     *   $http: for ajax call to api
     *   $state: to redirect to any required route
     *   $stateParams: Capture $state query string
     *   localStorageService: passing data to local storage
     */
    constructor($state, $http, localStorageService, $stateParams){
        //Decorator that wraps a class in a DI injector for the given dependencies.
        'ngInject';
        this.localStorageService = localStorageService;
        this.$http = $http;
        this.$state = $state;
        //Capture event id from query string
        this.eventId = $stateParams.eventId;
        //empty array for stands
        this.stands = [];
        //empty object of data
        this.data = {};
        //empty hall id to be filled by ajax response in onInit
        this.hallId = '';
        //default rows and columns in the hall
        this.rows = '3';
        this.cols = '3';
        //bind onCellClick bindings with this.onCellClick
        this.onCellClick = this.onCellClick.bind(this);
        //console.log(this.eventId);
        //console.log(this.localStorageService.get('token'));
        //console.log(this.localStorageService.get('admin'));
        //check if admin is logged in or not
        if(this.localStorageService.get('admin') == 0 && !this.localStorageService.get('token'))
        {
            this.$state.go('home');
        }
    }

    //Method to run on initialization of a component
    $onInit() {
        //Ajax call promise to get all halls for an event in database
        this.$http({
            methd: 'GET',
            url: 'api/halls/'+this.eventId,
            //JWT token fetched on signin or signup
            headers: {
                'Authorization': 'bearer '+this.localStorageService.get('token')
            }
            //es6 arrow function used
        }).then(response => {
            this.hallId =   response.data.data[0].id;
            this.rows   =   response.data.data[0].rows;
            this.cols   =   response.data.data[0].cols;
        })
        //catch error if any from api
            .catch(error => {
                console.log(error);
            });
    }

    //onCellClick method calls every time cell clicked
    onCellClick(data) {
        //make cell active toggle
        const active = !data.active
        data.active = active;
        //if cell active add cell's data to data array
        if (active) {
            this.data[`${data.x}-${data.y}`] = data;
        } else {
            //else remove cell information from data array
            delete this.data[`${data.x}-${data.y}`];
        }
    }

    //hallManaged method called when all stands added and hall managed button clicked
    hallManaged() {
        //create data by iterating data array to create new API requested data object
        const data = Object.keys(this.data).map(k => ({
            price: this.data[k].price,
            x: this.data[k].x,
            y: this.data[k].y
        }));
        //service call to add available stands with information
        this.$http({
            method: 'POST',
            url: 'api/stands/add/' + this.hallId + '/' + this.localStorageService.get('id'),
            headers: {
                'Authorization': 'bearer '+this.localStorageService.get('token')
            },
            data: data
        }).then(response => {
                //stands added redirect admin to admin map to add more events
                if (response.data.status == 'success') {
                    this.$state.go('adminMap');
                }
            })
            .catch(error => {
                //admin session expired redirect to signout
                if(error.data.error == 'token_expired')
                {
                    this.$state.go('signout');
                }
            })
    }
}

//export class to ui.router state and register a controller to angularjs app
export default HallCtrl;
//Class MapCtrl for map screen with map and add markers feature for admin.
//location: resources/assets/js/app/modules/admin/map
class MapCtrl{

    //Load constructor with
    /*
     *   $state: to redirect to any required route
     *   $http: for ajax call to api
     *   localStorageService: passing data to local storage
     */
    constructor($state, $http, localStorageService){
        //Decorator that wraps a class in a DI injector for the given dependencies.
        'ngInject';
        //bind onMapClick bindings with this.onMapClick
        this.onMapClick = this.onMapClick.bind(this);
        //bind onMarkerClick bindings with this.onMarkerClick
        this.onMarkerClick = this.onMarkerClick.bind(this);
        this.localStorageService = localStorageService;
        this.$http = $http;
        this.$state = $state;
        //data with empty location to be filled on event creation.
        this.data = {location:''};
        //Markers empty array to be filled on every event creation
        this.markers = [];
        //console.log(this.localStorageService.get('token'));
        //console.log(this.localStorageService.get('admin'));
        //Check if user logged in
        if(!this.localStorageService.get('token'))
        {
            this.$state.go('home');
        }
        //Check if logged in user is admin
        else if(this.localStorageService.get('admin') == 0)
        {
            this.$state.go('home');
        }
    }

    //onFormSubmit method called on event creation and submit
    onFormSubmit(data) {
        this.markers = [];
        //API call to add new event on map
        this.$http({
            method: 'POST',
            url: 'api/event/add',
            headers: {
                'Authorization': 'bearer '+this.localStorageService.get('token')
            },
            data: data
        }).then(response => {
            //if success redirects to admin hall of event with event id to add stands
            if(response.data.status == 'success')
                {
                    this.$state.go('adminHall', { eventId:  response.data.data.id});
                }
            })
            .catch(error => {
                //session expire redirects to signout
                if(error.data.error == 'token_expired')
                {
                    this.$state.go('signout');
                }
            })
    }

    //onMapClick method invoke with map(latlng) and event detail when admin clicks on map
    onMapClick(map, event) {
        //remove any previously open markers
        this.removeMarkers();
        //Gather event detail from marker infor window
        const $content = angular.element(require('./info-window.html'));
        //add submit call on click in infowindow submit form
        $content.find('.submit').click(() => {
            this.onFormSubmit({
                title: $content.find('.title').val(),
                rows: $content.find('.rows').val(),
                cols: $content.find('.cols').val(),
                startdate: $content.find('.startDate').val(),
                enddate: $content.find('.endDate').val(),
                latitude: event.latLng.lat(),
                longitude: event.latLng.lng(),
                location: this.data.location,
                user_id: this.localStorageService.get('id')
            });
            //close infowindow on form submit
            infoWindow.close();
        });
        //applyt datepiker to date-picker class for start date and end date
        $($content.find('.date-picker')).datepicker({
            autoHide:true,
            format:'yyyy-mm-dd'
        });

        //set info window for newly created event with event information
        const infoWindow = new google.maps.InfoWindow({
            content: $content[0],
            maxWidth: 500,
        });

        //add marker to event latlng
        const marker = new google.maps.Marker({
            position: event.latLng,
            map,
        });

        //update markers array
        this.markers.push(marker);

        //google latlng to address api
        const geocoder = new google.maps.Geocoder;
        //get event's readable address by converting latlng
        geocoder.geocode({'location': {lat:event.latLng.lat(), lng: event.latLng.lng()}},  (results, status) => {
            if (status === 'OK') {
                this.data.location = results[1].formatted_address;
            }
        });

        infoWindow.open(map, marker);
    }
    //removeMarkers method to be called when new marker added to remove unset markers
    removeMarkers() {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(null);
        }
    }

    //onMarkerClick method to be called when click on marker
    onMarkerClick() {
        this.removeMarkers();
    }
}

//export class to ui.router state and register a controller to angularjs app
export default MapCtrl;
//Class SignoutCtrl for signing out user form application.
//location: resources/assets/js/app/modules/home
class SignoutCtrl {
    //Load constructor with
    /*
     *   $state: for redirection
     *   localStorageService: passing data to local storage
     */
    constructor ($state, localStorageService){
        //Clear all local storage
        localStorageService.clearAll();
        //redirect user to home screen to signin again or new signup
        $state.go('home');
    }
}
//export class to ui.router state and register a controller to angularjs app
export default SignoutCtrl;
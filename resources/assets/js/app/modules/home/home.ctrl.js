//Class HomeCtrl for home screen with signup and sign in options.
//location: resources/assets/js/app/modules/home
class HomeCtrl {
    //Load constructor with
    /*
     *   $state: for redirection
     *   localStorageService: passing data to local storage
     */
    constructor($state, localStorageService){
        //Decorator that wraps a class in a DI injector for the given dependencies.
        'ngInject';
        this.$state = $state;
        //if localstorage token is set  than check user role is admin or not
        if(localStorageService.get('token'))
        {
            //if localstorage admin flag is off
            if(localStorageService.get('admin') == 0) {
                //redirect normal user to view clients map for events and book stands
                this.$state.go('eventsMap');
            }else
            {
                //redirect admin to amdin map for creating new events
                this.$state.go('adminMap');
            }
        }
    }
}
//export class to ui.router state and register a controller to angularjs app
export default HomeCtrl;
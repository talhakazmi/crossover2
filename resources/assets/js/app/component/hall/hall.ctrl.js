//Load hall grid individual scss
import './styles.scss';
//Class HallCtrl for loading event hall with grid of stands.
//location: resources/assets/js/app/component/hall
class HallCtrl {
    constructor() {
        //Decorator that wraps a class in a DI injector for the given dependencies.
        'ngInject';
        //initially empty grid array to be used in hall grid creation.
        this.grid = [];
    }
    //Method to run on initialization of a component
    $onInit() {
        //bind onCellClick bindings with this.onCellClick
        this.onCellClick = this.onCellClick();
        //Defining empty object for available stands
        const available = {};
        //check for any active cells
        if (this.activeCell && this.activeCell.length) {
            //active cells added to the available stands empty object
            this.activeCell.forEach(cell => {
                available[`${cell.cell_row}-${cell.cell_col}`] = cell;
            });
        }
        //Define rows and columns for the current hall
        const rows = parseInt(this.rows);
        const cols = parseInt(this.cols);
        //The ng-style row height and cell width to fixed on a window.
        this.rowHeight = 100 / rows;
        this.cellWidth = 100 / cols;
        //If rows and columns are not zero
        if (rows && cols) {
            //iterate rows and columns and push available stands in grid array
            for (let x = 0; x < rows; x++) {
                const cells = [];
                for (let y = 0; y < cols; y++) {
                    cells.push(
                        available[`${x}-${y}`]
                            ? Object.assign({
                                    active: true,
                                }, available[`${x}-${y}`]
                            )
                            : {
                                x,
                                y,
                            }
                    );
                }
                //Push every available stand in a grid
                this.grid.push(cells);
            }
        }
    }
}
//export class to register  a component to angularjs app
export default HallCtrl;
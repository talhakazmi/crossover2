//Class HeaderCtrl for loading header with buttons to display across the application for user and admin.
//location: resources/assets/js/app/component/header
class HeaderCtrl {
    //Only redirection is to be needed for button clicks.
    constructor($state){
        this.$state = $state;
    }
}
//export HeaderCtrl angularJS app to register a component..
export default HeaderCtrl;
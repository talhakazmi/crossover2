//Class GmapCtrl for loading map with grid markers.
//location: resources/assets/js/app/component/gmap
class GmapCtrl {
    //Load constructor with
    /*
    *   $http: for ajax call to api
    *   $timeout: to execute click event immediately
    *   $element: accessing html element
    *   localStorageService: passing data to local storage
     */
    constructor($http, $timeout, $element, localStorageService) {
        //Decorator that wraps a class in a DI injector for the given dependencies.
        'ngInject';
        this.localStorageService = localStorageService;
        this.$http = $http;
        this.$timeout = $timeout;
        //Create a new function with 'this' bound to removeinfoWindows
        this.removeinfoWindows = this.removeinfoWindows.bind(this);
        //infowindows initialize with empty array to be filled by api
        this.infoWindows = [];
        //initialize google map instance to this.map
        this.map = new google.maps.Map($element.find('.gmap')[0], {
            zoom: 4,
            center: {lat: -25.363, lng: 131.044}
        });
    }

    //Method to run on initialization of a component
    $onInit() {
        //bind onClick bindings with this.onClick
        this.onClick = this.onClick();
        //bind onMarkerClick bindings with this.onMarkerClick
        this.onMarkerClick = this.onMarkerClick();
        //if this.onClick trigger
        if (this.onClick) {
            //add event listener on map click
            google.maps.event.addListener(this.map, 'click', event => {
                //remove all showing info windows
                this.removeinfoWindows();
                //event and map to onClick for using latlng and event detail
                this.onClick(this.map, event);
                this.$timeout();
            });
        }
        //Ajax call promise to get all events in database
        this.$http({
            methd: 'GET',
            url: 'api/events',
            //JWT token fetched on signin or signup
            headers: {
                'Authorization': 'bearer '+this.localStorageService.get('token')
            }
            //es6 arrow function used
        }).then(response => {
                //iterate api response
                for (let i = 0; i < response.data.length; i++) {
                    //load info window html
                    const $content = angular.element(require('./info-window.html'));
                    //set marker position by api response event
                    let myLatlng = new google.maps.LatLng(response.data[i].latitude, response.data[i].longitude);
                    //create marker on map at given position
                    const marker = new google.maps.Marker({
                        position: myLatlng,
                        map: this.map,
                    });
                    //update html content from api response
                    $content.find('.title').html(response.data[i].title);
                    $content.find('.location').html(response.data[i].location);
                    $content.find('.date').html(response.data[i].start_date + ' to ' + response.data[i].end_date);
                    //create info windows with updated content for every iterate
                    const infoWindow = new google.maps.InfoWindow({
                        content: $content[0],
                        maxWidth: 500,
                    });
                    //add created infowindows to infowindos empty array
                    this.infoWindows.push(infoWindow);
                    //add marker click event listner to every created marker
                    marker.addListener('click', () => {
                        //remove exoisting infowindows on marker click
                        this.removeinfoWindows();
                        //open current marker's infowindow
                        infoWindow.open(this.map, marker);
                        //pass data to onMarkerClick function if bound
                        if(this.onMarkerClick)
                        {
                            this.onMarkerClick(response.data[i]);
                            this.$timeout();
                        }
                    });
                }
            })
            //catch error if any from api
            .catch(error => {
                console.log(error);
            });
    }
    //remove infowindows method to remove every infowindow
    removeinfoWindows() {
        //iterate on infowindows array
        for (var i = 0; i < this.infoWindows.length; i++) {
            //set every infowindow null on map
            this.infoWindows[i].setMap(null);
        }
    }
}
//export class to register a component to angularjs app
export default GmapCtrl;
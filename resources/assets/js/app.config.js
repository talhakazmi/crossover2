//Import app.routes.js to load routes in config
import routes from './app.routes';
//export constructor with ngInject and routes providing stateProvider and urlRouterProvider in every route/controller.
export default ($stateProvider, $urlRouterProvider, localStorageServiceProvider) => {
    'ngInject';

    routes($stateProvider, $urlRouterProvider);
};
//Export constructor with state/url defining their template and controller.
export default ($stateProvider, $urlRouterProvider) => {
    $stateProvider
    /*
         * Home route to start using application
         * url: /home
         */
        .state('home', {
            url: '/home',
            template: require('./app/modules/home/home.html'),
            controller: 'HomeCtrl',
            controllerAs: '$ctrl'
        })
        /*
         * description: Admin signup route where admin can signup to the system
         * url: /admin/signup
         */
        .state('adminSignup', {
            url: '/admin/signup',
            template: require('./app/modules/admin/signup/signup.html'),
            controller: 'AdminSignupCtrl',
            controllerAs: '$ctrl'
        })
        /*
         *  description: signin route where user can signin to the system
         *  url: /signin
         */
        .state('signin', {
            url: '/signin',
            template: require('./app/modules/company/signin/signin.html'),
            controller: 'SigninCtrl',
            controllerAs: '$ctrl'
        })
        /*
         *  description: signup route where user can signup to the system
         *  url: /signup
         */
        .state('signup', {
            url: '/signup',
            template: require('./app/modules/company/signup/signup.html'),
            controller: 'SignupCtrl',
            controllerAs: '$ctrl'
        })
        /*
         *   description: sign out route where user can sign out from the system
         *   url: /signout
         */
        .state('signout', {
            url: '/signout',
            controller: 'SignoutCtrl',
            controllerAs: '$ctrl'
        })
        /*
         *  description: map route to display events to companies for booking
         *  url: /events/map
         */
        .state('eventsMap', {
            url: '/events/map',
            template: require('./app/modules/company/map/map.html'),
            controller: 'MapCtrl',
            controllerAs: '$ctrl'
        })
        /*
         *  description: events hall route for companies to book available stand.
         *  url: /events/hall/:eventId
         */
        .state('eventsHall', {
            url: '/events/hall/:eventId',
            template: require('./app/modules/company/hall/hall.html'),
            controller: 'HallCtrl',
            controllerAs: '$ctrl'
        })
        /*
         *  description: hall stand route where Company can provide company's information.
         *  url: /events/stand/:id/:eventId
         */
        .state('eventsStand', {
            url: '/events/stand/:id/:event',
            template: require('./app/modules/company/stand/stand.html'),
            controller: 'StandCtrl',
            controllerAs: '$ctrl'
        })
        /*
         *  description: Manage stand route for companies to manage stand and add visitor during event.
         *  url: /manage/stand
         */
        .state('manage', {
            url: '/manage/stand',
            template: require('./app/modules/company/manage/manage.html'),
            controller: 'ManageCtrl',
            controllerAs: '$ctrl'
        })
        /*
         *  description: Add visitor's information form route
         *  url: /manage/visitor/:id
         */
        .state('visitor', {
            url: '/manage/visitor/:id',
            template: require('./app/modules/company/manage/visitor.html'),
            controller: 'VisitorCtrl',
            controllerAs: '$ctrl'
        })
        /*
         *  description: Admin map route where admin can add event to the system
         *  url: /amdin/map
         */
        .state('adminMap', {
            url: '/admin/map',
            template: require('./app/modules/admin/map/map.html'),
            controller: 'AdminMapCtrl',
            controllerAs: '$ctrl'
        })
        /*
         *  description: Admin hall route where admin can add available stands in the event
         *  url: /admin/hall/:eventId
         */
        .state('adminHall', {
            url: '/admin/hall/:eventId',
            template: require('./app/modules/admin/hall/hall.html'),
            controller: 'AdminHallCtrl',
            controllerAs: '$ctrl'
        })
    /*
     default route to home.
     */
    $urlRouterProvider.otherwise('/home');
};
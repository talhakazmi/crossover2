<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    /**
     * Get the visitor that owns the stand.
     */
    public function stand()
    {
        //visitors belongs to stand
        return $this->belongsTo('App\Models\Stand');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    /**
     * Get the hall for the event.
     */
    public function halls()
    {
        //event has many halls
        return $this->hasMany('App\Models\Hall');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stand extends Model
{
    /**
     * Get the hall that owns the stand.
     */
    public function hall()
    {
        //stand belongs to hall
        return $this->belongsTo('App\Models\Hall');
    }

    /**
     * Get the user that owns the stand.
     */
    public function user()
    {
        //stand belongs to user
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Get the visitors for the stand.
     */
    public function visitors()
    {
        //stand has many visitors
        return $this->hasMany('App\Models\Visitor');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hall extends Model
{
    /**
     * Get the stands for the hall.
     */
    public function stands()
    {
        //hall has many stands
        return $this->hasMany('App\Models\Stand');
    }

    /**
     * Get the event that owns the hall.
     */
    public function event()
    {
        //hall belongs to an event
        return $this->belongsTo('App\Models\Event');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Hall;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /*
     * get all events in database
     */
    public function all(){
        $events = Event::all();
        //response return in JSON format
        return response()->json($events);
    }

    /*
     * Add new event to database
     */
    public function add(Request $request){

        //Create new event instance
        $event = new Event;
        //Map event detail from request to ORM
        $event->user_id = $request->input('user_id');
        $event->title = $request->input('title');
        $event->location = $request->input('location');
        $event->latitude = $request->input('latitude');
        $event->longitude = $request->input('longitude');
        $event->start_date = $request->input('startdate');
        $event->end_date = $request->input('enddate');

        //save new event detail
        $event->save();

        //add hall for given stand
        $hall = new Hall;
        //get event id from query string
        $hall->event_id = $event->id;
        //static rows and columns for a hall
        $hall->rows = $request->input('rows');
        $hall->cols = $request->input('cols');
        //save hall in an event
        $hall->save();

        //return response in JSON with status
        return response()->json(['status'=>'success','data'=>$event]);
    }

    //Get hall information of given event
    public function get_hall($event_id)
    {
        //get hall by event id
        $hall = Hall::where('event_id',$event_id)->get();

        //if hall don't exists
        if($hall->count() <= 0)
        {
            //return status failed in response with error no hall defined
            return response()->json(['status'=>'failed','error'=>'No hall defined']);
        }
        //return respons ein JSON with status and stands detail
        return response()->json(['status'=>'success','data'=>$hall]);
    }
}

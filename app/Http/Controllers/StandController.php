<?php

namespace App\Http\Controllers;

use App\Models\Hall;
use App\Models\Stand;
use App\Models\Visitor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StandController extends Controller
{
    /*
     * Add stand in databnase for any event with admin userID
     */
    public function add($hall_id, $user_id, Request $request){

        //iterate over request to qadd stands in newly added hall
        foreach($request->input() as $val) {
            //create new instance for stand model
            $stand = new Stand;
            //map stand values against DB keys
            $stand->hall_id = $hall_id;
            $stand->cell_row = $val['x'];
            $stand->cell_col = $val['y'];
            $stand->price = $val['price'];
            $stand->user_id = $user_id;
            //save stand in hall
            $stand->save();
        }
        //return response in JSON with status
        return response()->json(['status'=>'success','data'=>$hall_id]);
    }
    /*
     * Get all stands with detail for a given event
     */
    public function get_all($event_id){

        //get hall by event id
        $hall = Hall::where('event_id',$event_id)->get();
        //if hall don't exists
        if($hall->count() <= 0)
        {
            //return status failed in response with error no hall defined
            return response()->json(['status'=>'failed','error'=>'No hall defined']);
        }
        //get stands for a hall
        $stands =   Hall::find($hall[0]->id)->Stands;
        //return respons ein JSON with status and stands detail
        return response()->json(['status'=>'success','data'=>$stands]);
    }
    /*
     * Update stand information with company provided information
     */
    public function update($stand_id, Request $request)
    {
        //validations applied on book stand form fileds
        $rules = [
            'company' => 'required',
            'email' => 'required|email',
            'document' => 'required|max:10000|mimes:doc,docx',
            'logo' => 'required|max:10000|image'
        ];

        $field = ['company', 'email', 'document', 'logo'];
        //only following fields are expected by validator anything else discarded
        $input = $request->only($field);
        $validate = Validator::make($input, $rules);
        //if validatoin passes
        if (!$validate->fails()) {
            //save marketing document in folder
            $docPath = $request->document->storeAs($request->input('company') . '/' . $stand_id, $request->document->getClientOriginalName());
            //save company logo in folder
            $logoPath = $request->logo->storeAs($request->input('company') . '/' . $stand_id, $request->logo->getClientOriginalName());
            //add stand information in Database
            $stand = Stand::where('id', $stand_id)->update([
                'company' => $request->input('company'),
                'email' => $request->input('email'),
                'document' => $docPath,
                'logo' => $logoPath,
                'user_id' => $request->input('user_id')
            ]);
            //return rtesponse in JSON with status
            return response()->json(['status' => 'success', 'data' => $stand]);

        }
        else {
            //if validation fails return response in JSOn with status failed and validation error
            return response()->json(['status' => 'failed','error'=>$validate->errors()]);
        }
    }
    /*
     * add visitors to the stand on the day of event
     */
    public function addVisitor($stand_id, Request $request){
        //apply validatoin on add visitor form fields
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'occupation' => 'required'
        ];

        $field = ['name', 'email', 'occupation'];
        //only expected fields are validated anything else will be discarded
        $input = $request->only($field);
        $validate = Validator::make($input, $rules);
        //if validation passes
        if (!$validate->fails()) {
            $visitor = new Visitor;
            //add visitor to Database with stand id
            $visitor->stand_id = $stand_id;
            $visitor->name = $request->input('name');
            $visitor->email = $request->input('email');
            $visitor->occupation = $request->input('occupation');
            //save visitors information
            $visitor->save();
            //return response in JSON with status
            return response()->json(['status' => 'success', 'data' => $visitor]);
        }
        else {
            //if validation fails return response in JSON with status failed and validation error
            return response()->json(['status' => 'failed','error'=>$validate->errors()]);
        }
    }
    /*
     * get stands information by userID for manage stands or add visitors to the stand
     */
    public function get($user_id)
    {
        //fetch stands with user id in relation with hall and event detail
        $stands = Stand::where('user_id',$user_id)->with('hall.event')->get();
        //return response in JSON with status
        return response()->json(['status'=>'success','data'=>$stands]);
    }
}

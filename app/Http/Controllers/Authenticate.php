<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;


class Authenticate extends Controller
{
    public function signin(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['status'=>'failed','error' => ['email'=>['Invalid Credentials']]]);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['status'=>'failed','error' => ['email'=>['could_not_create_token']]]);
        }

        $user   =   User::where('email',$request->input('email'))->withCount('stands')->get();

        $token = compact('token');
        // all good so return the token
        return response()->json(['status'=>'success','token'=>$token['token'],'data'=>$user]);
    }

    public function signup(Request $request){
        $rules = [
                'email' => 'required|email|unique:users',
                'name' => 'required|string|min:6',
                'password' => 'required|min:6'
                ];
        $field = ['name','email','password'];
        $input = $request->only($field);
        $validate = Validator::make($input,$rules);

        if(!$validate->fails())
        {
            $user = new User;

            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));

            if($request->input('is_admin'))
            {
                $user->is_admin = 1;
            }

            $user->save();

            $userWithStands =   User::where('id',$user->id)->withCount('stands')->get();

            return response()->json(['status'=>'success','token'=>JWTAuth::fromUser($user),'data'=>$userWithStands]);
        }
        else
        {
            return response()->json(['status'=>'failed','error'=>$validate->errors()]);
        }
    }
}

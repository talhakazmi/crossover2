<?php

namespace App\Console\Commands;

use App\Models\Event;
use App\Models\Hall;
use App\Models\Stand;
use App\Models\Visitor;
use Illuminate\Console\Command;

class SendEventClosureEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:event';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Event closure email to stand owner';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $halls = Hall::whereHas('events', function ($query) {
            $query->where('end_date', '>=', date('Y-m-d'));
        })->get();

        foreach($halls as $hall)
        {
            $stands = Stand::where('hall_id', $hall->id)->get();
        }

        foreach($stands as $stand){
            $visitors = Visitor::where('stand_id',$stand->id);

            // Send the email to user
            Mail::queue('event', ['company' => $stand,'visitors' => $visitors], function ($mail) use ($stand) {
                $mail->to($stand['email'])
                    ->from('admin@company.com', 'Crossover')
                    ->subject('Visitors to your stand');
            });
        }
    }
}

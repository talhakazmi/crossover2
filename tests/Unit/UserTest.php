<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * Sign up success test.
     *
     * @return void
     */
    public function testSignupSuccess()
    {
        $response = $this->call('POST', 'api/signup', ['name'=>'Testing', 'email' => str_random(6).'@gmail.com', 'password' => 'qazxsw']);
        $responseData   =   json_decode($response->getContent());

        $this->assertEquals('success', $responseData->status);
    }

    /**
     * Sign up failure - record already exist test.
     *
     * @return void
     */
    public function testSignupFailureAlreadyExist()
    {
        $user = User::all()->last();

        $response = $this->call('POST', 'api/signup', ['name'=>'Testing', 'email' => $user->email, 'password' => $user->password]);
        $responseData   =   json_decode($response->getContent());

        $this->assertEquals('failed', $responseData->status);
        $this->assertEquals('The email has already been taken.', $responseData->error->email[0]);
    }

    /**
     * Sign up failure - name length too short test.
     *
     * @return void
     */
    public function testSignupFailureNameLengthTooShort()
    {
        $response = $this->call('POST', 'api/signup', ['name'=>'Test', 'email' => str_random(6).'@gmail.com', 'password' => 'qazxsw']);
        $responseData   =   json_decode($response->getContent());

        $this->assertEquals('failed', $responseData->status);
        $this->assertEquals('The name must be at least 6 characters.', $responseData->error->name[0]);
    }

    /**
     * Sign up failure - password length too short test.
     *
     * @return void
     */
    public function testSignupFailurePasswordLengthTooShort()
    {
        $response = $this->call('POST', 'api/signup', ['name' => 'Testing', 'email' => str_random(6) . '@gmail.com', 'password' => 'qasw']);
        $responseData = json_decode($response->getContent());

        $this->assertEquals('failed', $responseData->status);
        $this->assertEquals('The password must be at least 6 characters.', $responseData->error->password[0]);
    }

    /**
     * Sign in success test.
     *
     * @return void
     */
    public function testSigninSuccess()
    {
        //Fetch existing user to sign in
        $user = User::all()->last();

        $response = $this->call('POST', 'api/signin', ['email' => $user->email, 'password' => 'qazxsw']);
        $responseData   =   json_decode($response->getContent());

        $this->assertEquals('success', $responseData->status);
    }

    /**
     * Sign in failure test.
     *
     * @return void
     */
    public function testSigninFailure()
    {
        $response = $this->call('POST', 'api/signin', ['email' => 'talhakaz@gmail.com', 'password' => '654321']);
        $responseData   =   json_decode($response->getContent());

        $this->assertEquals('failed', $responseData->status);
    }
}

<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class EventTest extends TestCase
{
    /**
     * Event success creation test.
     *
     * @return void
     */
    public function testEventCreationSuccess()
    {
        $user = new User();

        $user->name = 'admin';
        $user->email = 'admin@project.com';
        $user->password = bcrypt('654321');

        $user->save();

        //$user = User::all()->last();

        $user = $this->call('POST', 'api/signin', ['email' => $user->email, 'password' => '654321']);
        $userData   =   json_decode($user->getContent());

        $response = $this->call('POST', 'api/event/add?token='.$userData->token, [

            'user_id'=>$userData->data[0]->id,
            'title'=>'TestEvent',
            'location'=>'karachi, pakistan',
            'latitude'=>'24.35214',
            'longitude'=>'64.35214',
            'startdate'=>'2017-01-29',
            'enddate'=>'2017-02-01',

        ]);
        $responseData = json_decode($response->getContent());

        $this->assertEquals('success', $responseData->status);
    }

    /**
     * Event list fetched for map test.
     *
     * @return void
     */
    public function testEventSuccess()
    {
        $user = User::all()->last();

        $user = $this->call('POST', 'api/signin', ['email' => $user->email, 'password' => '654321']);
        $userData   =   json_decode($user->getContent());

        $response = $this->call('GET', 'api/events?token='.$userData->token);
        $responseData = json_decode($response->getContent());

        $this->assertNotEmpty($responseData);
    }
}

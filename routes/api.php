<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('jwt.auth')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/signin', 'Authenticate@signin');
Route::post('/signup', 'Authenticate@signup');
Route::get('/image/{company}/{stand}/{file}', function($company,$stand,$file){
    $path = storage_path() . '/app/companies/' . $company.'/'.$stand.'/'.$file;

    if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;

});

Route::get('/document/{company}/{stand}/{file}', function($company,$stand,$file){
    $path = storage_path() . '/app/companies/' . $company.'/'.$stand.'/'.$file;

    if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;

});

Route::middleware('jwt.auth')->get('/events', 'EventController@all');
Route::middleware('jwt.auth')->post('/event/add', 'EventController@add');
Route::middleware('jwt.auth')->get('/halls/{event_id}', 'EventController@get_hall');
Route::middleware('jwt.auth')->post('/stands/add/{hall_id}/{user_id}', 'StandController@add');
Route::middleware('jwt.auth')->get('/stands/get/{event_id}', 'StandController@get_all');
Route::middleware('jwt.auth')->post('/stands/update/{stand_id}', 'StandController@update');
Route::middleware('jwt.auth')->post('/stands/visitor/{stand_id}', 'StandController@addVisitor');
Route::middleware('jwt.auth')->get('/stands/{user_id}', 'StandController@get');
